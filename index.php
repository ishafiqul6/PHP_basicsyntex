<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>This is php Basic tag </title>
        <style>
            body {background-color:lightgrey}
            h1   {color:red; text-align: center}
            h2   {color:blue; text-align: center}
            h3   {color:greenyellow; text-align: center}
       
        </style>
    </head>
    <body>
        <?php
            echo "<h1>This is first tag for php</h1> <br>";
        ?>
        <? 
       echo "<h2>This is short tag for php</h2> <br>";
        ?>
        <%
        echo "<h3>This is ASP tag for php</h3> <br>";
        %>
       <script language="php">
           echo "<h1 >This is script  tag for php</h1> <br>";
       </script>
    </body>
</html>
